const template = document.createElement('template')

template.innerHTML = `
    <style>
     :host([hidden]) { display: none }
     :host {
	 --color-link: blue;
     }
     :host {
	 display: flex;
	 position: absolute;
	 top: 0.5rem;
	 left: 0.5rem;
	 z-index: 1;
	 background-color: white;
     }
     nav {
	 padding: 0.2rem;
     }
     nav-item {
	 display: flex;
     }
     nav-item a {
	 padding: 1rem;
	 text-decoration: none;
	 color: var(--color-link);
     }
    </style>
    <nav horizontal class="Component"></nav>
`

const NavMain = class extends HTMLElement {
    constructor() {
	super()
	this.attachShadow({mode: 'open'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    async connectedCallback() {
	this.title = this.getAttribute('title') || 'Main navigation'
	this.render()
    }
    render() {
	let $component = this.shadowRoot.querySelector('.Component')

	let navItem = document.createElement('nav-item')

	let homeLink = document.createElement('a')
	homeLink.innerHTML = 'Exit'
	homeLink.href = '../'
	homeLink.title = 'Navigate to the homepage, and exit survey'
	navItem.appendChild(homeLink)

	$component.appendChild(navItem)
    }
}

customElements.define('nav-main', NavMain)

export default NavMain
