const template = document.createElement('template')

template.innerHTML = `
    <style>
     :host([hidden]) { display: none }
     :host {
	 box-sizing: border-box;
	 --color-valid: green;
	 --color-bg-host: #5f0030;
	 --color-bg-button: lightgray;
	 --color-bg-button-submit: white;
     }
     :host {
	 display: flex;
	 padding-top: 2rem;
	 padding-bottom: 2rem;
	 padding-right: 2rem;
	 padding-left: 2rem;
	 background-color: var(--color-bg-host);
	 
	 position: fixed;
	 bottom: 0;
	 right: 0;
     }
     .Component {
	 flex-grow: 1;
	 display: flex;
	 justify-content: center;
     }
     button {
		 	font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, sans-serif;

	 background-color: var(--color-bg-button);
	 padding: 0.5rem;
	 font-size: 1.2rem;
	 cursor: pointer;
	 border-width: 0.3rem;
	 border-color: lightgray;
	 margin-left: 1rem;
     }
     button:focus,
     button:active {
	 border-color: gold;
     }
     button[type="submit"] {
	 background-color: var(--color-bg-button-submit);
     }
    </style>
    <div class="Component"></div>
`

const SurveyControls = class extends HTMLElement {
    constructor() {
	super()
	this.attachShadow({mode: 'open'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    async connectedCallback() {
	this.title = this.getAttribute('title')
	this.index = this.getAttribute('index')
	this.render()
    }
    submitAnswer = () => {
	const event = new CustomEvent('submitSurvey', {
	    bubbles: true
	})
	this.dispatchEvent(event)
    }
    nextQuestion = () => {
	const event = new CustomEvent('nextQuestion', {
	    bubbles: true
	})
	this.dispatchEvent(event)
    }
    render() {
	let $component = this.shadowRoot.querySelector('.Component')

	let nextButton = document.createElement('button')
	nextButton.innerHTML = '&rarr; Næste'
	nextButton.onclick = this.nextQuestion
	nextButton.title = 'Næste'
	$component.appendChild(nextButton)

	let submitButton = document.createElement('button')
	submitButton.innerHTML = 'Send my answer'
	submitButton.type = 'submit'
	submitButton.onclick = this.submitAnswer
	submitButton.title = 'Send the answers of to complete the survey.'
	$component.appendChild(submitButton)
    }
}

customElements.define('survey-controls', SurveyControls)

export default SurveyControls
